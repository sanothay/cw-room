package com.commonsware.todo.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ToDoDatabaseTest {
  @get:Rule
  val instantExecutorRule = InstantTaskExecutorRule()

  private val context =
    InstrumentationRegistry.getInstrumentation().targetContext
  private val db = ToDoDatabase.newTestInstance(context)
  private val underTest = db.todoStore()

  @Test
  fun basicCRUD() = runBlockingTest {
    val entities = arrayOf(
      ToDoEntity(description = "this is a test", isCompleted = true),
      ToDoEntity(description = "this is another test"),
      ToDoEntity(description = "this is... wait for it... yet another test")
    )

    val testJob = launch {
      underTest.save(*entities)

      assertThat(underTest.all().first(), hasItems(*entities))

      underTest.delete(*entities)

      assertThat(underTest.all().first().size, equalTo(0))
    }

    testJob.join()
  }
}