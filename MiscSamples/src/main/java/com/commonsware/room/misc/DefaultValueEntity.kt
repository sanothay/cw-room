/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.*

data class PartialDefaultValue(
  val id: String,
  val title: String
)

@Entity(tableName = "defaultValue")
class DefaultValueEntity(
  @PrimaryKey
  val id: String,
  val title: String,
  @ColumnInfo(defaultValue = "something")
  val text: String? = null,
  @ColumnInfo(defaultValue = "123")
  val version: Int = 1
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM defaultValue")
    fun loadAll(): List<DefaultValueEntity>

    @Query("SELECT * FROM defaultValue where id = :id")
    fun findByPrimaryKey(id: String): DefaultValueEntity

    @Insert
    fun insert(entity: DefaultValueEntity)

    @Query("INSERT INTO defaultValue (id, title) VALUES (:id, :title)")
    fun insertByQuery(id: String, title: String)

    @Insert(entity = DefaultValueEntity::class)
    fun insertPartial(partial: PartialDefaultValue)
  }
}