/*
  Copyright (c) 2017-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.pragmatest

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [City::class], version = 1)
abstract class CityDatabase : RoomDatabase() {
  abstract fun cityStore(): City.Store

  companion object {
    @VisibleForTesting
    const val DB_NAME = "un.db"

    fun newInstance(ctxt: Context, applyPragmas: Boolean): CityDatabase {
      val builder = Room.databaseBuilder(
        ctxt,
        CityDatabase::class.java,
        DB_NAME
      )

      if (applyPragmas) {
        builder.addCallback(object : Callback() {
          override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            db.query("PRAGMA journal_mode = MEMORY")
          }

          override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            db.query("PRAGMA synchronous = OFF")
          }
        })
      }

      return builder.build()
    }
  }
}